<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\Setup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Cms\Model\BlockFactory;
use Linets\Setup\Helper\Data;

class CreateBlocks implements DataPatchInterface, PatchRevertableInterface
{
    const STORE_CODE = 'default';


    const BLOCKS_DATA = [
        [
            'title' => 'Package Footer',
            'identifier' => 'package-footer',
            'content' => '
                    <style>#html-body [data-pb-style=S6BMEDE]{display:flex;flex-direction:column}#html-body [data-pb-style=H1EGWXH]{display:flex;flex-direction:column;width:8.33333333%;align-self:stretch}#html-body [data-pb-style=C1WO47Q],#html-body [data-pb-style=KEUJAYW],#html-body [data-pb-style=QC842TP],#html-body [data-pb-style=X8XO5WD]{display:flex;flex-direction:column;width:16.66666667%;align-self:stretch}#html-body [data-pb-style=QC842TP]{width:41.66666667%}#html-body [data-pb-style=SYK2UMD],#html-body [data-pb-style=WY0LQ01]{display:inline-block}#html-body [data-pb-style=ULPRWVV]{width:100%}#html-body [data-pb-style=IVMOUO8],#html-body [data-pb-style=LKTCY4B],#html-body [data-pb-style=U2GRUP1],#html-body [data-pb-style=ULPRWVV]{display:flex;flex-direction:column;align-self:stretch}#html-body [data-pb-style=IVMOUO8]{width:NaN%}</style><div class="footer-cont" data-content-type="row" data-appearance="full-width" data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="main" data-pb-style="S6BMEDE"><div class="row-full-width-inner" data-element="inner"><div class="pagebuilder-column-group" style="display: flex;" data-content-type="column-group" data-grid-size="12" data-element="main"><div class="pagebuilder-column logo-column show-on-desktop" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="H1EGWXH"><div data-content-type="text" data-appearance="default" data-element="main"><p><img id="FOO2SR8" style="width: 75px; height: 79px;" src="{{media url=&quot;wysiwyg/close-up-of-businessmen-handshake-in-the-office_1_.jpg&quot;}}" alt="" width="70" height="71"></p></div></div><div class="pagebuilder-column footer-column" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="X8XO5WD"><div class="footer-title" data-content-type="text" data-appearance="default" data-element="main">
                    <p>
                        <span style="font-size: 14px;"><strong><span style="font-family: HalisGR;"><span style="color: #ffffff;"><span style="font-family: HalisGR, Avenir;">COMPRAS</span></span></span></strong></span>
                    </p></div><div class="footer-block" data-content-type="text" data-appearance="default" data-element="main"><ul>
                    <li><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #e6e9ed;"><a style="color: #e6e9ed;" href="#">producto1</a></span></li>
                    <li>
                    <p><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #e6e9ed;"><a style="color: #e6e9ed;" href="#">producto2</a></span></p>
                    </li>
                    <li>
                    <p><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #e6e9ed;"><a style="color: #e6e9ed;" href="#">producto3</a></span></p>
                    </li>
                    <li><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #e6e9ed;"><a style="color: #e6e9ed;" href="#">producto4</a></span></li>
                    <li>
                    <p><a href="#">producto5</a></p>
                    </li>
                    <li><a href="#">producto6</a></li>
                    </ul></div></div><div class="pagebuilder-column footer-column" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="C1WO47Q"><div class="footer-title" data-content-type="text" data-appearance="default" data-element="main">
                    <p>
                        <span style="font-size: 14px; font-family: HalisGR, Avenir;"><strong><span style="color: #ffffff;">RUSTY</span></strong></span>
                    </p></div><div class="footer-block" data-content-type="text" data-appearance="default" data-element="main"><ul>
                    <li><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #e6e9ed;"><a style="color: #e6e9ed;" href="#">pagina1</a></span></li>
                    <li>
                    <p><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #e6e9ed;"><a style="color: #e6e9ed;" title="Nuestra Cultura" href="#">pagina2</a></span></p>
                    </li>
                    <li>
                    <p><a href="#">pagina3</a></p>
                    </li>
                    <li><a href="#">pagina4</a></li>
                    <li><a href="#">pagina5</a></li>
                    </ul></div></div><div class="pagebuilder-column footer-column" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="KEUJAYW"><div class="footer-title" data-content-type="text" data-appearance="default" data-element="main">
                    <p>
                        <span style="font-size: 14px; font-family: HalisGR, Avenir;"><strong><span style="color: #ffffff;">AYUDA</span></strong></span>
                    </p></div><div class="footer-block" data-content-type="text" data-appearance="default" data-element="main"><ul>
                    <li><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #e6e9ed;"><a style="color: #e6e9ed;" tabindex="0" href="/quienes-somos">quienes somos</a></span></li>
                    <li>
                    <p><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #e6e9ed;"><a style="color: #e6e9ed;" tabindex="0" title="tracking" href="/tracking">Estado del Pedido</a></span></p>
                    </li>
                    <li>
                    <p><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #e6e9ed;"><a style="color: #e6e9ed;" tabindex="0" href="#">Envíos y Entregas</a></span></p>
                    </li>
                    <li>
                    <p><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #e6e9ed;"><a style="color: #e6e9ed;" tabindex="0" title="custom-page" href="/custom-page">custom page</a></span></p>
                    </li>
                    <li>
                    <p><a href="#">custom page 2</a></p>
                    </li>
                    <li><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #e6e9ed;"><a style="color: #e6e9ed;" tabindex="0" href="/tiendas">Comunicate con Nosotros</a></span></li>
                    </ul></div></div><div class="pagebuilder-column" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="QC842TP"><div class="footer-title" data-content-type="text" data-appearance="default" data-element="main">
                    <p>
                        <span style="font-size: 14px; font-family: HalisGR, Avenir;"><strong><span style="color: #ffffff;">ÚNETE AL EQUIPO</span></strong></span>
                    </p></div><div data-content-type="text" data-appearance="default" data-element="main">
                    <p>
                        <span style="font-family: HalisGR, Avenir; color: #e6e9ed;">Regístrate y recibe nuestras promociones, productos o descuentos.</span>
                    </p></div><div class="show-on-desktop" data-content-type="divider" data-appearance="default" data-element="main"><hr data-element="line" data-pb-style="SYK2UMD"></div><div class="footer-title find-us" data-content-type="text" data-appearance="default" data-element="main">
                    <p>
                        <span style="font-size: 14px; font-family: HalisGR, Avenir;"><strong><span style="color: #ffffff;">ENCUÉNTRANOS EN:</span></strong></span>
                    </p></div><div class="find-us find-us-icons" data-content-type="text" data-appearance="default" data-element="main"><p><a href="https://www.facebook.com/volcomlatam/?ref=page_internal"> <img id="W8QCFT1" style="width: 30px; height: 30px;" src="{{media url=&quot;wysiwyg/close-up-of-businessmen-handshake-in-the-office_1_.jpg&quot;}}" alt="" width="30" height="30"> </a> &nbsp;&nbsp;<a href="{{media url=&quot;wysiwyg/close-up-of-businessmen-handshake-in-the-office_1_.jpg&quot;}}"> <img style="width: 30px; height: 30px;" src="{{media url=&quot;wysiwyg/close-up-of-businessmen-handshake-in-the-office_1_.jpg&quot;}}" alt="" width="30" height="30"> </a> &nbsp;&nbsp;<a href="https://twitter.com/volcomlatam?lang=es"> <img style="width: 30px; height: 30px;" src="{{media url=&quot;wysiwyg/close-up-of-businessmen-handshake-in-the-office_1_.jpg&quot;}}" alt="" width="30" height="30"> </a>&nbsp;&nbsp; <a href="https://www.youtube.com/user/RustyStoneYAE"> <img style="width: 30px; height: 30px;" src="{{media url=&quot;wysiwyg/close-up-of-businessmen-handshake-in-the-office_1_.jpg&quot;}}" alt="" width="30" height="30"> </a></p></div><div data-content-type="text" data-appearance="default" data-element="main"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sodales condimentum mauris, eu bibendum tellus iaculis ultrices. Suspendisse feugiat laoreet nisl</p></div></div></div><div class="show-on-desktop" data-content-type="divider" data-appearance="default" data-element="main"><hr data-element="line" data-pb-style="WY0LQ01"></div><div class="pagebuilder-column-group" style="display: flex;" data-content-type="column-group" data-grid-size="12" data-element="main"><div class="pagebuilder-column show-on-desktop" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="ULPRWVV"><div data-content-type="text" data-appearance="default" data-element="main"><p style="text-align: center;"><a href="#"><span style="font-family: HalisGR, Avenir; color: #e6e9ed;">Cambio y Devoluciones</span></a></p></div></div><div class="pagebuilder-column show-on-desktop" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="U2GRUP1"><div data-content-type="text" data-appearance="default" data-element="main"><p style="text-align: center;"><a href="#"><span style="font-family: HalisGR, Avenir; color: #e6e9ed;">Término y Condiciones</span></a></p></div></div><div class="pagebuilder-column show-on-desktop" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="LKTCY4B"><div data-content-type="text" data-appearance="default" data-element="main"><p style="text-align: center;"><a href="#"><span style="font-family: HalisGR, Avenir; color: #e6e9ed;">Políticas de Privacidad</span></a></p></div></div><div class="pagebuilder-column copyright-column" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="IVMOUO8"><div class="show-on-desktop" data-content-type="text" data-appearance="default" data-element="main"><p style="text-align: right;"><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #ffffff;">© custom. Todos los derechos reservados.</span></p></div><div class="show-on-mobile" data-content-type="text" data-appearance="default" data-element="main"><p style="text-align: center;"><span style="font-family: HalisGR, Avenir; color: #ffffff; font-size: 10px;"><span style="font-size: 14px; font-family: HalisGR, Avenir; color: #ffffff;"><span style="font-size: 12px;">© custom. Todos los derechos reservados.</span></span></span></p></div><div class="show-on-mobile" data-content-type="text" data-appearance="default" data-element="main">
                    <p style="text-align: center;">
                        <span style="font-family: HalisGR, Avenir; color: #ffffff; font-size: 8px;">Cambio y Devoluciones&nbsp; &nbsp;&nbsp;Término y Condiciones&nbsp; &nbsp;&nbsp;Políticas de Privacidad</span>
                    </p></div></div></div></div></div>
                ',
        ],
        [
            'title' => 'PDP Payment Product View (Linets)',
            'identifier' => 'cms_custom_payment_block',
            'content' => '
                <style>#html-body [data-pb-style=G0R9LKG]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}</style><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="G0R9LKG"><div data-content-type="html" data-appearance="default" data-element="main">&lt;div class="custom_payment_block"&gt;
                &lt;strong&gt;Pagando con&lt;/strong&gt;
                &lt;div class="payments"&gt;
                &lt;img src="{{media url=wysiwyg/tarjetas.png}}" alt="" /&gt;
                &lt;/div&gt;
                &lt;/div&gt;</div></div></div>
            ',
        ],
        [
            'title' => 'Footer Newsletter Title (Linets)',
            'identifier' => 'cms_footer_newsletter_title',
            'content' => '
                <style>#html-body [data-pb-style=KXUY1AM]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}</style><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="KXUY1AM"><div data-content-type="text" data-appearance="default" data-element="main"><p><strong>Suscribite y recibí descuentos exclusivos en tu casilla de correo</strong></p></div></div></div> 
            ',
        ],
        [
            'title' => 'Footer Newsletter Container Bottom (Linets)',
            'identifier' => 'cms_footer_links_container_bottom',
            'content' => '
                    <style>#html-body [data-pb-style=THQENLH]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}</style><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="THQENLH"><div data-content-type="html" data-appearance="default" data-element="main">&lt;div class="footer-newsletter-bottom"&gt;
                    &lt;img src="{{media url=wysiwyg/tarjetas.png}}" alt="" /&gt;
                    &lt;ul&gt;
                        &lt;li&gt;&lt;a href="javascript:void(0);"&gt;Empresa&lt;/a&gt;&lt;/li&gt;
                        &lt;li&gt;&lt;a href="javascript:void(0);"&gt;Productos&lt;/a&gt;&lt;/li&gt;
                        &lt;li&gt;&lt;a href="javascript:void(0);"&gt;Servicios&lt;/a&gt;&lt;/li&gt;
                        &lt;li&gt;&lt;a href="javascript:void(0);"&gt;Contacto&lt;/a&gt;&lt;/li&gt;
                    &lt;/ul&gt;
                &lt;/div&gt;</div></div></div>   
            ',
        ],
        [
            'title' => 'Footer Links Container (Linets)',
            'identifier' => 'cms_footer_links_container',
            'content' => '
                <style>#html-body [data-pb-style=J601T37]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}</style><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="J601T37"><div data-content-type="html" data-appearance="default" data-element="main">&lt;div class="footer-links-container"&gt;
                &lt;a href="/" class="footer-logo"&gt;&lt;img src="{{media url=wysiwyg/logo_linets.png}}" alt="" /&gt;&lt;/a&gt;
                &lt;ul class="footer-information"&gt;
                &lt;li&gt;Tu dirección&lt;/li&gt;
                &lt;li&gt;&lt;a href="mailto:example@tuempresa.com.ar"&gt;example@tuempresa.com.ar&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href="tel:+541122222222"&gt;+5411 2222 . 2222&lt;/a&gt;&lt;/li&gt;
                &lt;/ul&gt;
                &lt;ul class="footer-social-links"&gt;
                &lt;li&gt;&lt;a class="link-instagram" href="javascript:void(0);" target="_blank"&gt;&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a class="link-facebook" href="javascript:void(0);" target="_blank"&gt;&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a class="link-twitter" href="javascript:void(0);" target="_blank"&gt;&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a class="link-pinterest" href="javascript:void(0);" target="_blank"&gt;&lt;/a&gt;&lt;/li&gt;
                &lt;/ul&gt;
                &lt;/div&gt;</div></div></div>   
            ',
        ],
        [
            'title' => 'Header top msg (Linets)',
            'identifier' => 'cms-header-top-msg',
            'content' => '
                <style>#html-body [data-pb-style=KN7ACTM]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}</style><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="KN7ACTM"><div data-content-type="html" data-appearance="default" data-element="main">&lt;div class="header-top-msg"&gt;
                &lt;span class="header-top-msg-shipping"&gt;&lt;strong&gt;{{trans "ENVÍO GRATIS " }} &lt;/strong&gt;&lt;/span&gt;
                &lt;span class="header-top-msg-txt"&gt;{{trans "EN COMPRAS SUPERIORES A  " }} &lt;/span&gt;
                &lt;span class="header-top-msg-price"&gt;$ XXXX &lt;/span&gt;
                &lt;/div&gt;</div></div></div>
            ',
        ]
    ];

    /**
     * MenuBlockCreator constructor.
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        BlockFactory $blockFactory,
        Data $helper
    ) {
        $this->_blockFactory = $blockFactory;
        $this->_helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        // Asignación de datos para actualizar/crear bloques cms
        $data = self::BLOCKS_DATA;
        $storeId = $this->_helper->getStoreByCode(self::STORE_CODE);
        if ($storeId) {
            for ($i=0; $i < count($data); $i++) {
                $this->_helper->updateCreateBlock($data[$i]['title'], $data[$i]['identifier'], $storeId, $data[$i]['content']);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        /**
         * No dependencies for this
         */
        return [];
    }

    /**
     * Delete the block
     */
    public function revert()
    {
        $data = self::BLOCKS_DATA;
        for ($i=0; $i < count($data); $i++) {
            $headerNoticeBlock = $this->_blockFactory->create()->load($data[$i]['identifier'], 'identifier');

            if($headerNoticeBlock->getId()) {
                $headerNoticeBlock->delete();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        /**
         * Aliases are useful if we change the name of the patch until then we do not need any
         */
        return [];
    }
}

