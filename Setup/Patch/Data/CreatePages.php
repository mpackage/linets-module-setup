<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\Setup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Cms\Model\PageFactory;
use Linets\Setup\Helper\Data;

class CreatePages implements DataPatchInterface, PatchRevertableInterface
{
    const STORE_CODE = 'default';

    const DATA = [
        [
            'title' => 'package 404 Not Found',
            'layout' => 'cms-full-width',
            'identifier' => 'pkg-404-not-found',
            'content' => '
                    <style>#html-body [data-pb-style=I6TBC8T]{justify-content:center;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll;text-align:center}#html-body [data-pb-style=GDM2W1S]{border-style:none}#html-body [data-pb-style=ADCD6O2],#html-body [data-pb-style=J416BYQ]{max-width:100%;height:auto}@media only screen and (max-width: 768px) { #html-body [data-pb-style=GDM2W1S]{border-style:none} }</style><div data-content-type="row" data-appearance="contained" data-element="main"><div class="not-found-404" data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="I6TBC8T"><h2 data-content-type="heading" data-appearance="default" data-element="main">Ups! Página no encontrada</h2><figure data-content-type="image" data-appearance="full-width" data-element="main" data-pb-style="GDM2W1S"><img class="pagebuilder-mobile-hidden" src="{{media url=wysiwyg/error-404.png}}" alt="" title="" data-element="desktop_image" data-pb-style="J416BYQ"><img class="pagebuilder-mobile-only" src="{{media url=wysiwyg/error-404.png}}" alt="" title="" data-element="mobile_image" data-pb-style="ADCD6O2"></figure><div data-content-type="text" data-appearance="default" data-element="main"><p style="text-align: center;">La página que buscas no está disponible,<br>cambio de link o la eliminamos.<br>¡Disculpa las molestias!</p>
                    <p style="text-align: center;">Vuelve al&nbsp;<a id="NE-2-a" href="/">Home</a> o elige una ruta para volver</p></div></div></div>
                '
        ],

        [
            'title' => 'Cambios y Devoluciones Package',
            'layout' => 'cms-full-width',
            'identifier' => 'pkg-cambios-y-devoluciones',
            'content' => '
                <style>#html-body [data-pb-style=HW9C9PN]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}</style><div data-content-type="row" data-appearance="contained" data-element="main"><div class="cambiosydevoluciones" data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="HW9C9PN"><h2 data-content-type="heading" data-appearance="default" data-element="main">Política de Cambios y Devoluciones</h2><div data-content-type="text" data-appearance="default" data-element="main"><p>¡Gracias por comprar en (Nombre de la tienda)!</p>
                <p>Ofrecemos reembolso y/o cambio dentro de los primeros 30 días de tu compra. Si han transcurrido 30 días desde tu compra, no se te ofrecerá un reembolso y/o cambio de ningún tipo.</p>
                <p><strong>Elegibilidad para reembolsos y cambios</strong></p>
                <ul>
                <li>
                <p>Tu artículo debe estar sin usar y en las mismas condiciones en que lo recibió.</p>
                </li>
                <li>
                <p>El artículo debe estar en el embalaje original.</p>
                </li>
                <li>
                <p>Para completar tu devolución, requerimos un recibo o comprobante de compra.</p>
                </li>
                <li>
                <p>Solo se pueden reembolsar los artículos de precio regular; los artículos de venta no se pueden reembolsar.</p>
                </li>
                <li>
                <p>Si el artículo en cuestión fue marcado como un regalo cuando fue comprado y se envió directamente a ti, recibirás un crédito de regalo por el valor de tu devolución.</p>
                </li>
                </ul>
                <p><strong>Cambios</strong>&nbsp;<em>(si es aplicable)</em></p>
                <p>Solo reemplazamos los artículos si están defectuosos o dañados. Si necesitas cambiarlo por el mismo artículo, envíanos un email a (Agrega dirección de correo electrónico relevante) y envía tu artículo a: (Dirección relevante).</p>
                <p><strong>Bienes Exentos</strong></p>
                <p>Los siguientes bienes están exentos de reembolsos:</p>
                <ul>
                <li>
                <p>Tarjetas de regalo (gif cards).</p>
                </li>
                <li>
                <p>Algunos artículos de salud y cuidado personal.</p>
                </li>
                </ul>
                <p><strong>Partial refunds are granted</strong>&nbsp;<em>(si es aplicable)</em></p>
                <ul>
                <li>
                <p>Cualquier artículo que no se encuentre en su estado original, esté dañado o falte una parte por razones que no se debieron a nuestro error.</p>
                </li>
                <li>
                <p>Cualquier artículo que se devuelva más de 30 días después de la entrega.</p>
                </li>
                </ul>
                <p>Una vez que se reciba e inspeccione tu devolución, te enviaremos un email para notificarte que hemos recibido tu artículo devuelto. También te notificaremos la aprobación o el rechazo de tu reembolso.</p>
                <p>Si es aprobado, entonces se procesará tu reembolso y se aplicará un crédito automáticamente a tu tarjeta de crédito o método de pago original, dentro de una cierta cantidad de días.</p>
                <p><strong>Reembolsos atrasados ​​o faltantes</strong></p>
                <ul>
                <li>
                <p>Si aún no has recibido un reembolso, primero verifica otra vez tu cuenta bancaria. Luego, comunícate con la compañía de tu tarjeta de crédito. Puede tomar algún tiempo antes de que se publique oficialmente tu reembolso.</p>
                </li>
                <li>
                <p>Si has hecho todo esto y aún no has recibido tu reembolso, comunícate con nosotros al (email y/o número de teléfono).</p>
                </li>
                </ul>
                <p><strong>Envío</strong></p>
                <ul>
                <li>
                <p>Por favor, no devuelvas el producto al fabricante. Debe enviarse a la siguiente dirección (dirección relevante).</p>
                </li>
                <li>
                <p>Usted será responsable de pagar sus propios costos de envío para devolver su artículo.</p>
                </li>
                <li>
                <p>Los costos de envío no son reembolsables. Si recibes un reembolso, el costo del envío de devolución se deducirá de tu reembolso.</p>
                </li>
                <li>
                <p>Dependiendo de dónde vivas, el tiempo que tarde tu producto cambiado en llegar a ti puede variar.</p>
                </li>
                <li>
                <p>Por favor, nota que no podemos garantizar que recibiremos tu artículo devuelto.</p>
                </li>
                </ul></div></div></div>
            '
        ],
        [
            'title' => 'Terminos y Condiciones Package',
            'layout' => 'cms-full-width',
            'identifier' => 'pkg-terminos-y-condiciones',
            'content' => '
                <style>#html-body [data-pb-style=MW8NSL1]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}</style><div data-content-type="row" data-appearance="contained" data-element="main"><div class="terminosycondiciones" data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="MW8NSL1"><h2 data-content-type="heading" data-appearance="default" data-element="main">(Nombre de la tienda) Política de Términos y Condiciones</h2><div data-content-type="text" data-appearance="default" data-element="main"><p>Bienvenido a (Nombre de la tienda). Estos términos y condiciones describen las reglas y regulaciones para el uso del sitio web (Nombre de la tienda).</p>
                <p><strong>(Nombre de la tienda) se encuentra en (Dirección).</strong></p>
                <p>Al acceder a este sitio web, asumimos que aceptas estos términos y condiciones en su totalidad. No continúes usando el sitio web (Nombre de la tienda) si no aceptas todos los términos y condiciones establecidos en esta página.</p>
                <p>La siguiente terminología se aplica a estos Términos y Condiciones, Declaración de Privacidad y Aviso legal y cualquiera o todos los Acuerdos: el&nbsp;<strong>Cliente</strong>,&nbsp;<strong>Usted</strong>&nbsp;y&nbsp;<strong>Su</strong>&nbsp;se refieren a usted, la persona que accede a este sitio web y acepta los términos y condiciones de la Compañía. La&nbsp;<strong>Compañía, Nosotros mismos, Nosotros y Nuestro</strong>, se refiere a nuestra Compañía.&nbsp;<strong>Parte, Partes o Nosotros</strong>, se refiere en conjunto al Cliente y a nosotros mismos, o al Cliente o a nosotros mismos.</p>
                <p>Todos los términos se refieren a la oferta, aceptación y consideración del pago necesario para efectuar el proceso de nuestra asistencia al Cliente de la manera más adecuada, ya sea mediante reuniones formales de una duración fija, o por cualquier otro medio, con el propósito expreso de conocer las necesidades del Cliente con respecto a la provisión de los servicios/productos declarados de la Compañía, de acuerdo con y sujeto a la ley vigente de (Dirección).</p>
                <p>Cualquier uso de la terminología anterior u otras palabras en singular, plural, mayúsculas y/o, él/ella o ellos, se consideran intercambiables y, por lo tanto, se refieren a lo mismo.</p>
                <p><strong>Cookies</strong></p>
                <p>Empleamos el uso de cookies. Al utilizar el sitio web de (Nombre de la tienda), usted acepta el uso de cookies de acuerdo con la política de privacidad de (Nombre de la tienda). La mayoría de los modernos sitios web interactivos de hoy en día usan cookies para permitirnos recuperar los detalles del usuario para cada visita.</p>
                <p>Las cookies se utilizan en algunas áreas de nuestro sitio para habilitar la funcionalidad de esta área y la facilidad de uso para las personas que lo visitan. Algunos de nuestros socios afiliados/publicitarios también pueden usar cookies.</p>
                <p><strong>Licencia</strong></p>
                <p>A menos que se indique lo contrario, (Nombre de la tienda) y/o sus licenciatarios les pertenecen los derechos de propiedad intelectual de todo el material en (Nombre de la tienda).</p>
                <p>Todos los derechos de propiedad intelectual están reservados. Puedes ver y/o imprimir páginas desde (Agrega URL) para tu uso personal sujeto a las restricciones establecidas en estos términos y condiciones.</p>
                <p>No debes:</p>
                <ul>
                <li>Volver a publicar material desde (Añadir URL).</li>
                <li>Vender, alquilar u otorgar una sub-licencia de material desde (Agregar URL).</li>
                <li>Reproducir, duplicar o copiar material desde (Agregar URL).</li>
                <li>Redistribuir contenido de (Nombre de la tienda), a menos de que el contenido se haga específicamente para la redistribución.</li>
                </ul>
                <p><strong>Aviso legal</strong></p>
                <p>En la medida máxima permitida por la ley aplicable, excluimos todas las representaciones, garantías y condiciones relacionadas con nuestro sitio web y el uso de este sitio web (incluyendo, sin limitación, cualquier garantía implícita por la ley con respecto a la calidad satisfactoria, idoneidad para el propósito y/o el uso de cuidado y habilidad razonables).</p>
                <p>Nada en este aviso legal:</p>
                <ul>
                <li>Limita o excluye nuestra o su responsabilidad por muerte o lesiones personales resultantes de negligencia.</li>
                <li>Limita o excluye nuestra o su responsabilidad por fraude o tergiversación fraudulenta.</li>
                <li>Limita cualquiera de nuestras o sus responsabilidades de cualquier manera que no esté permitida por la ley aplicable.</li>
                <li>Excluye cualquiera de nuestras o sus responsabilidades que no pueden ser excluidas bajo la ley aplicable.</li>
                </ul>
                <p>Las limitaciones y exclusiones de responsabilidad establecidas en esta Sección y en otras partes de este aviso legal:</p>
                <ol>
                <li>están sujetas al párrafo anterior; y</li>
                <li>rigen todas las responsabilidades que surjan bajo la exención de responsabilidad o en relación con el objeto de esta exención de responsabilidad, incluidas las responsabilidades que surjan en contrato, agravio (incluyendo negligencia) y por incumplimiento del deber legal.</li>
                </ol>
                <p>En la medida en que el sitio web y la información y los servicios en el sitio web se proporcionen de forma gratuita, no seremos responsables de ninguna pérdida o daño de ningún tipo.</p></div></div></div>
            '
        ],
        [
            'title' => 'Politicas de Privacidad Package',
            'layout' => 'cms-full-width',
            'identifier' => 'pkg-politicas-de-privacidad',
            'content' => '
                <style>#html-body [data-pb-style=NUC1KTJ]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}</style><div data-content-type="row" data-appearance="contained" data-element="main"><div class="politicas-privacidad" data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="NUC1KTJ"><h2 data-content-type="heading" data-appearance="default" data-element="main">(Nombre de la tienda) Política de Privacidad</h2><div data-content-type="text" data-appearance="default" data-element="main"><p>Esta Política de Privacidad describe cómo se recopila, utiliza y comparte tu información personal cuando visitas o realizas una compra en (URL de la tienda).</p>
                <p><strong>QUÉ INFORMACIÓN PERSONAL RECOPILAMOS</strong></p>
                <p>Cuando visitas el Sitio, recopilamos automáticamente cierta información sobre tu dispositivo, incluida información sobre tu navegador web, dirección IP, zona horaria y algunas de las cookies que están instaladas en tu dispositivo.</p>
                <p>Además, a medida que navegas por el Sitio, recopilamos información sobre las páginas web individuales o los productos que ves, qué sitios web o términos de búsqueda te remiten al Sitio, e información sobre cómo interactúas con el Sitio. Nos referimos a esta información recopilada automáticamente como&nbsp;<strong>Información del Dispositivo</strong>&nbsp;(device information).</p>
                <p>Recopilamos información del dispositivo utilizando las siguientes tecnologías:</p>
                <ul>
                <li>
                <p><strong>Cookies</strong>: son archivos de datos que se colocan en tu dispositivo o computadora y con frecuencia incluyen un identificador único anónimo.</p>
                </li>
                <li>
                <p><strong>Archivos de registro</strong>: rastrean las acciones que ocurren en el Sitio y recopilan datos, incluida tu dirección IP, el tipo de navegador, el proveedor de servicios de Internet, las páginas de referencia/salida y las marcas de fecha y hora.</p>
                </li>
                </ul>
                <p><em>Menciona todas las demás herramientas de seguimiento y/o tecnologías que usa tu sitio web.</em></p>
                <p>Además, cuando efectúas una compra o intentas realizar una compra a través del Sitio, recopilamos cierta información tuya, como tu nombre, dirección de facturación, dirección de envío, información de pago, incluidos números de tarjetas de crédito (<strong>menciona todos los tipos de pagos aceptados</strong>), dirección de email y el número de teléfono. Esto se denomina&nbsp;<strong>Información del Pedido</strong>.</p>
                <p><em>Asegúrate de mencionar toda otra información que recopiles.</em></p>
                <p>Al referirnos a&nbsp;<strong>Información Personal</strong>&nbsp;en esta Política de Privacidad, estamos hablando tanto de la Información del Dispositivo como de la Información del Pedido.</p>
                <p><strong>CÓMO USAMOS TU INFORMACIÓN PERSONAL</strong></p>
                <p>Utilizamos la Información de Pedido que recopilamos por lo general para cumplir con los pedidos realizados a través del Sitio (incluido el procesamiento de tu información de pago, la organización del envío y el envío de facturas y/o confirmaciones de pedidos).</p>
                <p>Además, usamos esta Información del Pedido para: comunicarnos contigo, examinar nuestros pedidos para detectar posibles riesgos o fraudes, para (en línea con las preferencias que has compartido con nosotros) ofrecerte información o publicidad relacionada con nuestros productos o servicios.</p>
                <p>Utilizamos la Información del Dispositivo que recopilamos para ayudarnos a detectar posibles riesgos y fraudes (en particular, tu dirección IP) y, en general, para mejorar y optimizar nuestro sitio.</p>
                <p><strong>COMPARTIENDO TU INFORMACIÓN PERSONAL</strong></p>
                <p>Compartimos tu Información Personal con terceros para ayudarnos a utilizarla como se describió anteriormente.</p>
                <p>También empleamos Google Analytics para ayudarnos a comprender cómo nuestros clientes usan (Nombre de la tienda).&nbsp;<a href="https://www.google.com/intl/en/policies/privacy/">Cómo usa Google tu Información Personal.</a>.</p>
                <p>Finalmente, también podemos compartir tu Información Personal para cumplir con las leyes y regulaciones aplicables, para responder a una citación, una orden de registro u otras solicitudes legales de información que recibimos, o para proteger nuestros derechos.</p>
                <p><strong>PUBLICIDAD DE COMPORTAMIENTO</strong></p>
                <p>Utilizamos tu Información Personal para proporcionarte anuncios específicos o comunicaciones de marketing que creemos que pueden ser de tu interés.</p>
                <p><em>Menciona los enlaces opt-out de servicios externos, tales como:</em></p>
                <ul>
                <li><a href="https://www.facebook.com/settings/?tab=ads">Facebook</a></li>
                <li><a href="https://www.google.com/settings/ads/anonymous">Google</a></li>
                </ul>
                <p>Puedes inhabilitar la publicidad dirigida…</p>
                <p><strong>TUS DERECHOS</strong></p>
                <p>Si eres un residente europeo, tienes derecho a acceder a la información personal que tenemos sobre ti y a solicitar que tu información personal se corrija, actualice o elimine. Si deseas ejercer este derecho, por favor contáctanos.</p>
                <p>Además, si eres un residente europeo, notamos que estamos procesando tu información para cumplir con los contratos que podríamos tener contigo (por ejemplo, si realizas un pedido a través del Sitio), o de otra manera para perseguir nuestros intereses comerciales legítimos mencionados anteriormente.</p>
                <p>Ten en cuenta que tu información se transferirá fuera de Europa, incluso a Canadá y Estados Unidos.</p>
                <p><strong>RETENCIÓN DE DATOS</strong></p>
                <p>Cuando realices un pedido a través del Sitio, mantendremos tu Información de Pedido para nuestros registros a menos que y hasta que nos solicites eliminar esta información.</p>
                <p><strong>MENORES</strong></p>
                <p>El Sitio no está destinado a personas menores de edad (EDAD CLARAMENTE MENCIONADA).</p>
                <p><strong>CAMBIOS</strong>&nbsp;Podemos actualizar esta política de privacidad de vez en cuando para reflejar, por ejemplo, cambios en nuestras prácticas o por otras razones operativas, legales o reglamentarias.</p>
                <p>Si tiene preguntas y/o necesitas más información, no dudes en ponerte en contacto con nosotros (<strong>Agrega información de contacto relevante</strong>).</p></div></div></div>
            '
        ],
        [
            'title' => 'Sobre nosotros Package',
            'layout' => 'cms-full-width',
            'identifier' => 'pkg-sobre-nosotros',
            'content' => '
                <style>#html-body [data-pb-style=I6TBC8T]{justify-content:center;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll;text-align:center}#html-body [data-pb-style=GDM2W1S]{border-style:none}#html-body [data-pb-style=ADCD6O2],#html-body [data-pb-style=J416BYQ]{max-width:100%;height:auto}@media only screen and (max-width: 768px) { #html-body [data-pb-style=GDM2W1S]{border-style:none} }</style><div data-content-type="row" data-appearance="contained" data-element="main"><div class="not-found-404" data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="I6TBC8T"><h2 data-content-type="heading" data-appearance="default" data-element="main">Ups! Página no encontrada</h2><figure data-content-type="image" data-appearance="full-width" data-element="main" data-pb-style="GDM2W1S"><img class="pagebuilder-mobile-hidden" src="{{media url=wysiwyg/error-404.png}}" alt="" title="" data-element="desktop_image" data-pb-style="J416BYQ"><img class="pagebuilder-mobile-only" src="{{media url=wysiwyg/error-404.png}}" alt="" title="" data-element="mobile_image" data-pb-style="ADCD6O2"></figure><div data-content-type="text" data-appearance="default" data-element="main"><p style="text-align: center;">La página que buscas no está disponible,<br>cambio de link o la eliminamos.<br>¡Disculpa las molestias!</p>
                <p style="text-align: center;">Vuelve al&nbsp;<a id="NE-2-a" href="/">Home</a> o elige una ruta para volver</p></div></div></div>
            '
        ],
        [
            'title' => 'Home Package',
            'layout' => 'cms-full-width',
            'identifier' => 'pkg-home',
            'content' => '
                <style>#html-body [data-pb-style=AKCQKE3]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=HSL29EE]{min-height:325px}#html-body [data-pb-style=E3YN4BG]{background-position:center top;background-size:cover;background-repeat:no-repeat;min-height:576px}#html-body [data-pb-style=U0Y34B9]{min-height:576px;background-color:transparent}#html-body [data-pb-style=GBWNQPI]{background-position:left top;background-size:cover;background-repeat:no-repeat;min-height:576px}#html-body [data-pb-style=YNK4E93]{min-height:576px;background-color:transparent}#html-body [data-pb-style=KUUA2AF],#html-body [data-pb-style=LKIO6JO]{display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=LKIO6JO]{justify-content:flex-start;background-color:#fafafa;padding-top:20px}#html-body [data-pb-style=KUUA2AF]{justify-content:center}#html-body [data-pb-style=GY8PK77]{text-align:center;margin-top:40px}#html-body [data-pb-style=ILF704Q]{text-align:center;margin-top:30px;margin-bottom:40px}#html-body [data-pb-style=PPK9CM5]{text-align:center}#html-body [data-pb-style=VW2SAAN]{margin-bottom:40px}#html-body [data-pb-style=W69JVDB],#html-body [data-pb-style=XW7KHHS]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=W69JVDB]{margin-top:40px}#html-body [data-pb-style=XW7KHHS]{width:50%;align-self:stretch}#html-body [data-pb-style=K3HJP6C]{border-style:none}#html-body [data-pb-style=JYCSJGQ],#html-body [data-pb-style=PSM40IY]{max-width:100%;height:auto}#html-body [data-pb-style=CIX0U4T]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll;width:50%;align-self:stretch}#html-body [data-pb-style=RVHJN3S]{border-style:none}#html-body [data-pb-style=DV0M7MG],#html-body [data-pb-style=ERI3G1J]{max-width:100%;height:auto}#html-body [data-pb-style=YUXEY5Y]{justify-content:flex-start;display:flex;flex-direction:column;background-position:left top;background-size:cover;background-repeat:no-repeat;background-attachment:scroll}#html-body [data-pb-style=DLK8JPY]{text-align:center;margin-top:60px}#html-body [data-pb-style=C4J80CB]{text-align:center;margin-top:30px;margin-bottom:40px}@media only screen and (max-width: 768px) { #html-body [data-pb-style=K3HJP6C],#html-body [data-pb-style=RVHJN3S]{border-style:none} }</style><div data-content-type="row" data-appearance="full-bleed" data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="main" data-pb-style="AKCQKE3"><div class="pagebuilder-slider" data-content-type="slider" data-appearance="default" data-autoplay="false" data-autoplay-speed="4000" data-fade="false" data-infinite-loop="true" data-show-arrows="true" data-show-dots="false" data-element="main" data-pb-style="HSL29EE"><div data-content-type="slide" data-slide-name="Slider1" data-appearance="poster" data-show-button="never" data-show-overlay="never" data-element="main"><div data-element="empty_link"><div class="pagebuilder-slide-wrapper" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/slider_1.png}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/slice1.png}}\&quot;}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="wrapper" data-pb-style="E3YN4BG"><div class="pagebuilder-overlay pagebuilder-poster-overlay" data-overlay-color="" data-element="overlay" data-pb-style="U0Y34B9"><div class="pagebuilder-poster-content"><div data-element="content"></div></div></div></div></div></div><div data-content-type="slide" data-slide-name="slide" data-appearance="poster" data-show-button="never" data-show-overlay="never" data-element="main"><div data-element="empty_link"><div class="pagebuilder-slide-wrapper" data-background-images="{\&quot;desktop_image\&quot;:\&quot;{{media url=wysiwyg/slider2.png}}\&quot;,\&quot;mobile_image\&quot;:\&quot;{{media url=wysiwyg/slice2.png}}\&quot;}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="wrapper" data-pb-style="GBWNQPI"><div class="pagebuilder-overlay pagebuilder-poster-overlay" data-overlay-color="" data-element="overlay" data-pb-style="YNK4E93"><div class="pagebuilder-poster-content"><div data-element="content"></div></div></div></div></div></div></div></div><div data-content-type="row" data-appearance="full-bleed" data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="main" data-pb-style="LKIO6JO"><div data-content-type="html" data-appearance="default" data-element="main">&lt;div class="info-home"&gt;
                &lt;ul&gt;
                &lt;li&gt;&lt;img src="{{media url=wysiwyg/envio.png}}" alt="" /&gt;&lt;/li&gt;
                &lt;li&gt;&lt;img src="{{media url=wysiwyg/mercado.png}}" alt="" /&gt;&lt;/li&gt;
                &lt;li&gt;&lt;img src="{{media url=wysiwyg/contacto.png}}" alt="" /&gt;&lt;/li&gt;
                &lt;/ul&gt;
                &lt;/div&gt;</div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="KUUA2AF"><h2 data-content-type="heading" data-appearance="default" data-element="main" data-pb-style="GY8PK77">Productos Destacados</h2><div data-content-type="products" data-appearance="grid" data-element="main" data-pb-style="ILF704Q">{{widget type="Magento\CatalogWidget\Block\Product\ProductsList" template="Magento_CatalogWidget::product/widget/content/grid.phtml" anchor_text="" id_path="" show_pager="0" products_count="4" condition_option="category_ids" condition_option_value="2" type_name="Catalog Products List" conditions_encoded="^[`1`:^[`aggregator`:`all`,`new_child`:``,`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`value`:`1`^],`1--1`:^[`operator`:`==`,`type`:`Magento||CatalogWidget||Model||Rule||Condition||Product`,`attribute`:`category_ids`,`value`:`2`^]^]" sort_order="position"}}</div><h2 class="block-title title" data-content-type="heading" data-appearance="default" data-element="main" data-pb-style="PPK9CM5">Marcas que nos acompañan</h2><div data-content-type="html" data-appearance="default" data-element="main" data-pb-style="VW2SAAN">&lt;div class="marcas"&gt;
                &lt;a href="javascript:void(0);"&gt;&lt;img src="{{media url=wysiwyg/bluecompany.jpg}}" alt="" /&gt;&lt;/a&gt;
                &lt;a href="javascript:void(0);"&gt;&lt;img src="{{media url=wysiwyg/cocha.jpg}}" alt="" /&gt;&lt;/a&gt;
                &lt;a href="javascript:void(0);"&gt;&lt;img src="{{media url=wysiwyg/domino.jpg}}" alt="" /&gt;&lt;/a&gt;
                &lt;a href="javascript:void(0);"&gt;&lt;img src="{{media url=wysiwyg/equifax.jpg}}" alt="" /&gt;&lt;/a&gt;
                &lt;a href="javascript:void(0);"&gt;&lt;img src="{{media url=wysiwyg/sky.jpg}}" alt="" /&gt;&lt;/a&gt;
                &lt;/div&gt;</div></div></div><div data-content-type="row" data-appearance="full-width" data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="main" data-pb-style="W69JVDB"><div class="row-full-width-inner" data-element="inner"><div class="pagebuilder-column-group" style="display: flex;" data-content-type="column-group" data-grid-size="12" data-element="main"><div class="pagebuilder-column" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="XW7KHHS"><figure data-content-type="image" data-appearance="full-width" data-element="main" data-pb-style="K3HJP6C"><img class="pagebuilder-mobile-hidden" src="{{media url=wysiwyg/banner1.png}}" alt="" title="" data-element="desktop_image" data-pb-style="PSM40IY"><img class="pagebuilder-mobile-only" src="{{media url=wysiwyg/banner1.png}}" alt="" title="" data-element="mobile_image" data-pb-style="JYCSJGQ"></figure></div><div class="pagebuilder-column" data-content-type="column" data-appearance="full-height" data-background-images="{}" data-element="main" data-pb-style="CIX0U4T"><figure data-content-type="image" data-appearance="full-width" data-element="main" data-pb-style="RVHJN3S"><img class="pagebuilder-mobile-hidden" src="{{media url=wysiwyg/banner2.png}}" alt="" title="" data-element="desktop_image" data-pb-style="ERI3G1J"><img class="pagebuilder-mobile-only" src="{{media url=wysiwyg/banner2.png}}" alt="" title="" data-element="mobile_image" data-pb-style="DV0M7MG"></figure></div></div></div></div><div data-content-type="row" data-appearance="contained" data-element="main"><div data-enable-parallax="0" data-parallax-speed="0.5" data-background-images="{}" data-background-type="image" data-video-loop="true" data-video-play-only-visible="true" data-video-lazy-load="true" data-video-fallback-src="" data-element="inner" data-pb-style="YUXEY5Y"><h2 data-content-type="heading" data-appearance="default" data-element="main" data-pb-style="DLK8JPY">Productos destacados</h2><div data-content-type="products" data-appearance="grid" data-element="main" data-pb-style="C4J80CB">{{widget type="Magento\CatalogWidget\Block\Product\ProductsList" template="Magento_CatalogWidget::product/widget/content/grid.phtml" anchor_text="" id_path="" show_pager="0" products_count="4" condition_option="category_ids" condition_option_value="2" type_name="Catalog Products List" conditions_encoded="^[`1`:^[`aggregator`:`all`,`new_child`:``,`type`:`Magento||CatalogWidget||Model||Rule||Condition||Combine`,`value`:`1`^],`1--1`:^[`operator`:`==`,`type`:`Magento||CatalogWidget||Model||Rule||Condition||Product`,`attribute`:`category_ids`,`value`:`2`^]^]" sort_order="position"}}</div></div></div>
            '
        ]

    ];

    /**
     * @param PageFactory $pageFactory
     */
    public function __construct(
        PageFactory $pageFactory,
        Data $helper
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        // Asignación de datos para actualizar/crear bloques cms
        $data = self::DATA;
        $storeId = $this->_helper->getStoreByCode(self::STORE_CODE);
        if ($storeId) {
            for ($i=0; $i < count($data); $i++) {
                $this->_helper->updateCreatePage($data[$i]['title'], $data[$i]['layout'], $data[$i]['identifier'], (array) $storeId, $data[$i]['content']);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        /**
         * No dependencies for this
         */
        return [];
    }

    /**
     * Delete the block
     */
    public function revert()
    {
        try {
            $data = self::DATA;
            for ($i=0; $i < count($data); $i++) {
                $page = $this->_pageFactory->create()->load($data[$i]['identifier'], 'identifier');

                if($page->getId()) {
                    $page->delete();
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        /**
         * Aliases are useful if we change the name of the patch until then we do not need any
         */
        return [];
    }
}

