<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Linets\Setup\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Cms\Model\BlockFactory $blockFactory
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     * @param \Magento\Store\Api\StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Store\Api\StoreRepositoryInterface $storeRepository
    ) {
        $this->_blockFactory = $blockFactory;
        $this->_pageFactory = $pageFactory;
        $this->_storeRepository = $storeRepository;
        parent::__construct($context);
    }


    public function updateCreateBlock ($title, $identifier, $stores, $content) {
        try {
            // Si se encuentra, va a actualizar el que ya existe
            $updateBlock = $this->_blockFactory->create()->load($identifier, 'identifier');
            if ($updateBlock->getId()) {
                echo "- Actualizando $title\n";
                $updateBlock->setIdentifier($identifier);
                $updateBlock->setTitle($title);
                $updateBlock->setIsActive(1);
                $updateBlock->setStores($stores);
                $updateBlock->setContent($content);
                $updateBlock->save();
            } else { // No se encuentra creado, crea un nuevo bloque
                echo "- Creando $title\n";
                $createBlock = $this->_blockFactory->create();
                $createBlock->setData([
                    'title' => $title,
                    'identifier' => $identifier,
                    'stores' => $stores,
                    'is_active' => 1,
                    'content' => $content
                ])->save();
            }
            return true;
        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
        }

    }


    public function updateCreatePage ($title, $layout, $identifier, $stores, $content) {
        try {
            // Si se encuentra, va a actualizar el que ya existe
            // Se agrega condificón que permita crear dos páginas con el mismo url-key, en distintos stores: (array)$stores === $updatePage->getStores()
            // Esto funciona sólo si no existe el url-key en el store en el que se intenta crear o en "all stores"
            $updatePage = $this->_pageFactory->create()->load($identifier, 'identifier');
            if ($updatePage->getId() && (array)$stores === $updatePage->getStores()) {
                echo "- Actualizando $title\n";
                $updatePage->setIdentifier($identifier);
                $updatePage->setTitle($title);
                $updatePage->setPageLayout($layout);
                $updatePage->setIsActive(1);
                $updatePage->setStores($stores);
                $updatePage->setContent($content);
                $updatePage->save();
            } else { // No se encuentra creado, crea un nuevo bloque
                $createPage = $this->_pageFactory->create();
                echo "- Creando $title\n";
                $createPage->setIdentifier($identifier);
                $createPage->setTitle($title);
                $createPage->setPageLayout($layout);
                $createPage->setIsActive(1);
                $createPage->setStores($stores);
                $createPage->setContent($content);
                $createPage->save();
            }
            return true;
        } catch (\Throwable $th) {
            echo $th->getMessage();
            return false;
        }
    }

    public function getStoreByCode ($storecode) {
        try {
            $store = $this->_storeRepository->get($storecode);
            return $store->getId();
        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
}
